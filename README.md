# NLP Sentiment140
I have started using a Kaggle Notebook before creating a model. That's why the commits were very rare after this point.  
Google Colab link: https://colab.research.google.com/drive/1_BD7gNK5D3e1TMd0VLXhL-BqC51DmJDu

## Links
1. https://www.kaggle.com/ngyptr/lstm-sentiment-analysis-keras
1. https://towardsdatascience.com/machine-learning-word-embedding-sentiment-classification-using-keras-b83c28087456
1. https://medium.com/@himanshu_23732/sentiment-analysis-with-sentiment140-e6b0c789e0ce
1. https://www.kaggle.com/paoloripamonti/twitter-sentiment-analysis